package com.onnet.mapptable.MapTable;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MapRepository extends JpaRepository<MapTable,Integer>         {

}
